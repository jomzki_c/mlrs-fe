const showSuccess = (message = 'Hi', description='This is sample', placement='topLeft') => {
    notification.success({
        message,
        description,
        placement,
    });
}

const showError = (message = 'Hi', description='This is sample', placement='topLeft') => {
    notification.error.open({
        message,
        description,
        placement,
    });
}


export default (ctx, inject) => {
    const notifFn = {
        showSuccess,
        showError
    };
  
    inject("notif", notifFn);
  
    ctx.$notif = notifFn;
  };
  