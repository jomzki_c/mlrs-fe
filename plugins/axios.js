export default function ({ $axios, redirect }) {
    $axios.interceptors.request.use(
        config => {
            let appData = localStorage.getItem('token');
            const token = appData;
            if (token) {
                config.headers['Authorization'] = 'Bearer ' + token;
            }
            return config;
        }, function (error) {
            // Do something with request error
            return Promise.reject(error);
        }
    );
}