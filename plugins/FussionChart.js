// import FusionCharts from "fusioncharts/core";
// // include chart from viz folder - import ChartType from fusioncharts/viz/[ChartType];
// import Column2D from "fusioncharts/viz/column2d";
// import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

// // add chart as dependency - FusionCharts.addDep(ChartType);
// FusionCharts.addDep(Column2D);
// FusionCharts.addDep(FusionTheme);
// import Vue from 'vue';
// import VueFusionCharts from 'vue-fusioncharts';
// import FusionCharts from 'fusioncharts';
// import Column2D from 'fusioncharts/fusioncharts.charts';
// import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

// Vue.use(VueFusionCharts, FusionCharts, Column2D, FusionTheme);

// const CombinationChart = (el = 'chart-container', chartConfig={}, data=[], categories = []) => {
//     let chartInstance = new FusionCharts({
//         type: "mscombi2d",
//         renderAt: el, // div container where chart will render
//         width: "800",
//         height: "400",
//         dataFormat: "json",
//         dataSource: {
//             // chart configuration
//             chart: chartConfig,
//             // chart categories
//             categories: categories,
//             // chart data
//             dataset: data
//         },
//     });

//     chartInstance.render();
// }

// export default (ctx, inject) => {
//     const chartFn = {
//         CombinationChart
//     };
  
//     inject("chart", chartFn);
  
//     ctx.$chart = chartFn;
//   };
  