import jwt_decoder from 'jwt-decode';


const decode = (token = localStorage.getItem('token')) => {
    let decoded = {};

    if(token != undefined){
        decoded = jwt_decoder(token);
        return decoded;
    } else return decoded;

}


export default (ctx, inject) => {
    const decoderFn = {
        decode
    };
  
    inject("decoder", decoderFn);
  
    ctx.$decoder = decoderFn;
  };
  