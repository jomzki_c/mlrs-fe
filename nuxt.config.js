module.exports =  {
  dev: process.env.NODE_ENV !== 'production',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    target: 'server',
    title: 'Molecular Laboratory System',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      {
        src: "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.js"
      },
      {
        src: "https://f001.backblazeb2.com/file/buonzz-assets/jquery.ph-locations-v1.0.0.js"
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/VueAntUi",
    "~/plugins/Notification",
    "~/plugins/JwtDecoder",
    "~/plugins/vuejsonexcel",
    "~/plugins/axios.js",
    {src: "@/plugins/ApexChart", ssr: false}
    // "~/plugins/FussionChart"
    // {src: "@/plugins/FussionChart", ssr: false}
    
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: `${process.env.API_BASE_URL}`,
    progress: false,
    headers: {
      common: {
        Accept: "*/*",
        "Content-Type": "application/json",
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
