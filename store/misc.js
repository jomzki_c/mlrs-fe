const { serialize } = require("~/lib/Serializer");
const { to } = require("await-to-js");

const base = "misc"
const baseGen = "generate"

export const actions = {

    async getUserTypes(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.get(`${base}/getUserTypes`, payload)
        );
        return serialize(err ? err.response : data);
    },
    async getBranches(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.get(`${base}/getBranches`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async getAddress(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.get(`${base}/getAddress`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async downloadGeneratedResult(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${baseGen}/download/result`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async downloadAntigenResult(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${baseGen}/download/antigenResult`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async downloadGeneratedForm(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${baseGen}/download/form`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async getDashboards(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/dashboard`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async getDashboardsMonthly(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/monthly/result`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async getDashboardsWeekly(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/weekly/result`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async generateReport(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${baseGen}/caseInvestigationReport`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async generateReportAntigen(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${baseGen}/caseInvestigationReport/antigen`, payload)
        );

        return serialize(err ? err.response : data);
    },
};