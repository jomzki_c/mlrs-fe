const { serialize } = require("~/lib/Serializer");
const { to } = require("await-to-js");

const base = "users"

export const actions = {

    async createUser(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/create`, payload)
        );
        return serialize(err ? err.response : data);
    },
    async userChangePassword(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/changePassword`, payload)
        );
        return serialize(err ? err.response : data);
    },
    async getUserInfoById(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/getUserById`, payload)
        );
        return serialize(err ? err.response : data);
    },
    async getUsersList(ctx) {
        
        const [err, data] = await to(
            this.$axios.get(`${base}/getUsersList`)
        );
        return serialize(err ? err.response : data);
    },
};