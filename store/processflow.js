const { serialize } = require("~/lib/Serializer");
const { to } = require("await-to-js");

const baseProcess = "processflow"

export const actions = {

    async getApplicationStatus(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${baseProcess}/getStatus`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async updateApplicationStatus(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${baseProcess}/updateStatus`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async preEncodeApplicationStatus(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${baseProcess}/preEncodeStatus`, payload)
        );

        return serialize(err ? err.response : data);
    },
    async updateSelectedApplicationStatus(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${baseProcess}/updateStatusMultiple`, payload)
        );

        return serialize(err ? err.response : data);
    },
};