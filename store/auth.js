const { serialize } = require("~/lib/Serializer");
const { to } = require("await-to-js");

const base = "auth"

export const actions = {

    async loginAccount(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/login`, payload)
        );
        return serialize(err ? err.response : data);
    },
    async passChangeFirst(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/firstLoginChange`, payload)
        );

        return serialize(err ? err.response : data);
    },
};