const { serialize } = require("~/lib/Serializer");
const { to } = require("await-to-js");

const base = "cif"

export const actions = {

    async createSpecimen(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/submit`, payload)
        );
        return serialize(err ? err.response : data);
    },
    async updateSpecimen(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/update`, payload)
        );
        return serialize(err ? err.response : data);
    },
    async duplicateSpecimen(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/duplicateApplication`, payload)
        );
        return serialize(err ? err.response : data);
    },
    async getPendings(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/getPendingList`, payload)
        );
        return serialize(err ? err.response : data);
    },
    async searchApplication(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.post(`${base}/searchBy/${payload.searchBy}`, payload.data)
        );
        return serialize(err ? err.response : data);
    },
    async getDetailsById(ctx, payload) {
        
        const [err, data] = await to(
            this.$axios.get(`${base}/getApplicationDetails/${payload}`)
        );
        return serialize(err ? err.response : data);
    },
};